from google.cloud import ndb
import csv
import random


class User(ndb.Model):
    first_name = ndb.StringProperty()
    last_name = ndb.StringProperty()
    email = ndb.StringProperty()
    ghost = ndb.KeyProperty(kind='Ghost')


def change_ghost(user_key, new_ghost):
    """
    args:
        * user_key: the key to get the entiry of a user
        * new_ghost: the key to get the new ghost
    return: the key to the updated user's entity
    """
    user = user_key.get()
    

    user.ghost = new_ghost

    return user.put()


def create_user(first_name, last_name, email, ghost):
    """
    args:
        * first_name: user's first name
        * last_name: user's last name
        * email: user's email address
        * ghost: the key of an allocated ghost
    return: user's key
    """
    new_user = User(first_name=first_name, last_name=last_name,
                    email=email, ghost=ghost)

    return new_user.put()

def create_user_assign_ghost(first_name,last_name, email, ghost_id):
    # create a new user entity
    ghost_ob = ndb.Key('Ghost', ghost_id)
    create_user(first_name=first_name, last_name=last_name,
                email=email, ghost=ghost_ob)
    pass

def get_all_users():
    """
    args: nothing
    return: a list of all users.
    """
    return User.query().fetch()


def get_users_by_email(email):
    """
    args:
        * email: user's email address
    return: a list of users correspond to the email address
    """
    return User.query().filter(User.email == email).fetch()


def create_test_user():
    ghost = get_random_ghosts(1)[0].key
    user = User(first_name="Anki", last_name="Yang",
                email="ankiyang@gmail.com", ghost=ghost)
    return user.put()


def get_test_user(user_key):
    return user_key.get()


class Ghost(ndb.Model):
    name = ndb.StringProperty(required=True)
    description = ndb.StringProperty()
    is_taken = ndb.BooleanProperty(required=True, default=False)


def get_all_ghost_info():
    """
    :return:    ghost name, ghost full name (firstname+ghost+last), email
    """
    data = []

    for ghost in get_all_ghosts():
        owner = lookup_owner(ghost.key)

        if not owner:
            email = ''
            ghost_full_name = ''
        else:
            email_info = owner[0].email,
            email = email_info[0]
            ghost_full_name = """%s "%s" %s""" % (
                owner[0].first_name, ghost.name, owner[0].last_name)

        data.append({
            'ghost_name': ghost.name,
            'email': email,
            'ghost_full_name': ghost_full_name
        })

    return data


def lookup_owner(ghost_key):
    """
    args:
        * ghost_key: the key of a ghost
    return: owners of the ghost. None will be returned if there is no owner.
    """
    owner = User.query().filter(User.ghost == ghost_key).fetch()

    return owner


def get_all_ghosts():
    """
    args: nothing
    return: a list of all ghosts.
    """
    return Ghost.query().fetch()


def get_available_ghosts():
    """
    args: nothing
    return: a list of ghosts which do not have owners.
    """
    return Ghost.query().filter(Ghost.is_taken == False).fetch()


def get_random_ghosts(num):
    """
    args:
        * num: number of ghosts.
    return: a list of available ghosts randomly choosen.
    """
    return random.sample(get_available_ghosts(), num)


def get_random_ghosts_name_id(num):
    """
    args:
        * num: number of ghosts.
    return: a list of available ghosts randomly choosen.
    """
    res = {i.name: i.key.id() for i in get_random_ghosts(num)}
    return res


def load_ghost_names():
    fw = open('PHANTOM_List.csv', 'r')
    data_read = csv.DictReader(fw, delimiter=',')
    ghost_data = [line for line in data_read]
    fw.close()
    return ghost_data


def create_ghost_entities():
    ghosts = []
    for each_ in load_ghost_names():
        ghost = Ghost(name=each_['Ghost name'],
                      description=each_['Description'])
        ghosts.append(ghost)
    ndb.put_multi(ghosts)


def query_all_ghost():
    client = ndb.Client()
    with client.context():
        qry = Ghost.query_all_ghost()
        return [(line.ghost_name, line.ghost_description) for line in qry]


if __name__ == '__main__':
    # create_GhostNameEntity()
    # load_ghost_names()
    # print(query_all_ghost())
    # create()



    with ndb.Client().context():
        create_test_user()
        # create_ghost_entities()
        
    #     ghost = ndb.Key('Ghost', 5707013812125696)
    #     print(lookup_owner(ghost).fetch())

# if __name__ == '__main__':
#     with ndb.Client().context():
#         print(get_all_ghost_info())
#         # key = create_test_user()

#         # time.sleep(5)

#         # print(key.get())

#         # users = get_all_users()
#         users = get_users_by_email("ankiyang@gmail.com")
#         print(users)
