from wtforms import Form, StringField
from wtforms.validators import DataRequired,length

class UserNameInput(Form):
    first_name = StringField('first_name', validators=[DataRequired()])
    last_name = StringField('last_name', validators=[DataRequired()])