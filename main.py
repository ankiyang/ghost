from flask import Flask, render_template, session,request, url_for, redirect
from authlib.integrations.flask_client import OAuth
from functools import wraps
from utils.input_forms import UserNameInput
from google.cloud import ndb
import securescaffold
from securescaffold.contrib.appengine import users

from utils.auth import GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET
from db.ghost_model import get_random_ghosts_name_id,get_all_ghost_info, create_user_assign_ghost


app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
# app = securescaffold.create_app(__name__)
# oauth = OAuth(app)
# google = oauth.register(
#     name='google',
#     client_id=GOOGLE_CLIENT_ID,
#     client_secret=GOOGLE_CLIENT_SECRET,
#     access_token_url='https://accounts.google.com/o/oauth2/token',
#     access_token_params=None,
#     authorize_url='https://accounts.google.com/o/oauth2/auth',
#     authorize_params=None,
#     api_base_url='https://www.googleapis.com/oauth2/v1/',
#     userinfo_endpoint='https://openidconnect.googleapis.com/v1/userinfo',  # This is only needed if using openId to fetch user info
#     client_kwargs={'scope': 'openid email profile'},
# )

@app.route('/')
def homepage():
    with ndb.Client().context():
        ghost_records = get_all_ghost_info()
    button_info = "Get a Phantom name"

    # user = users.get_current_user()
    # if user:
    #     email = user.email()
    #     user_id = user.user_id()
    #     button_info = "Change your Phantom name"

    return render_template('homepage.html',
                           records=ghost_records,
                           Button=button_info)


@app.route('/form', methods=['GET', 'POST'])
def name_form():
    print(dict(session))
    name_input_ = UserNameInput(request.form)
    is_validate = name_input_.validate()
    if request.method == 'POST':
        value_ = {
            'first_name': request.form.get('first_name'),
            'last_name': request.form.get('last_name')
        }
        session['user_input'] = value_

        if is_validate:
            return redirect(url_for("select_ghost_name"))
    # user = users.get_current_user()
    #
    # if user:
    #     email = user.email()
    #     user_id = user.user_id()
    #
    #     return "Hello signed-in user"
    return render_template('ghost_name_form.html')


@app.route('/select_ghost_name', methods=['GET', 'POST'])
def select_ghost_name():
    if request.method == 'GET':
        user_input = session.get('user_input')
        with ndb.Client().context():
            random_ghost_data = get_random_ghosts_name_id(3)
            random_ghost_data = random_ghost_data.copy()
        session['random'] = random_ghost_data

        random_ghost_names = random_ghost_data.keys()
        full_ghost_names = {ghost_: """%s "%s" %s""" % (user_input.get('first_name'), ghost_, user_input.get('last_name'))
                            for ghost_ in random_ghost_names}
        return render_template('name_result.html', results=full_ghost_names)

    if request.method == 'POST':
        user_input = session.get('user_input')
        selected_ghost_name = request.form.get('selected')

        with ndb.Client().context():
            create_user_assign_ghost(user_input.get('first_name'),
                                        user_input.get('last_name'),
                                        email="",
                                        ghost_id=session.get('random')[selected_ghost_name]
                                        )

        return redirect(url_for("homepage"))


if __name__ == '__main__':
    app.run(debug=True, port="8080")
